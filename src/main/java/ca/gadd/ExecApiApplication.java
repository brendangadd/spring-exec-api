package ca.gadd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * This whole thing assumes that your tarballs won't be long-lived. If they need
 * to stay around for a very long time, then you'll probably want to instead
 * upload them to some sort of distributed storage.
 * 
 * This is the quick and dirty "straight on the server" version.
 */
@SpringBootApplication
public class ExecApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExecApiApplication.class, args);
	}

	public ThreadPoolTaskExecutor taskExecutor() {
		var taskExecutor = new ThreadPoolTaskExecutor();
		// TODO: Max pool size should be a configuration property. And there are
		// plenty more properties you can tune.
		taskExecutor.setMaxPoolSize(4);
		return taskExecutor;
	}
}
