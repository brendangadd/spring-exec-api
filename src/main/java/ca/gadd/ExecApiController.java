package ca.gadd;

import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.ExecutionException;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@RestController
@RequestMapping("/api/exec")
public class ExecApiController {
  private ThreadPoolTaskExecutor taskExecutor;

  public ExecApiController(ThreadPoolTaskExecutor taskExecutor) {
    this.taskExecutor = taskExecutor;
  }

  /**
   * TODO: This should handle, not throw {@link IOException}.
   * TODO: This whole thing could be split into separate asynchronus requests so
   * that it isn't blocking the HTTP handler threads while creating the tar. If
   * you do that, you might be able to use @Async or something to leverage the
   * built-in executor pool (instead of the one I set up).
   * 
   * @param req The parsed POST body coming from the client.
   * @return HTTP response containing the resulting tar.
   * @throws IOException If... like... anything goes wrong.
   */
  @PostMapping("/tar")
  public ResponseEntity<StreamingResponseBody> tar(@RequestBody ExecTarRequest req) throws IOException {
    // TODO: Read the Javadoc for StreamingResponseBody! You should tune its
    // configuration, which I'm not doing at all here.
    try {
      var tarFuture = taskExecutor.submit(new TarTask(req.message));
      // TODO: Consider tarFuture.get(long, TimeUnit) to add a timeout.
      final var tar = tarFuture.get();

      return ResponseEntity.ok()
          .contentType(MediaType.APPLICATION_OCTET_STREAM)
          // If you find your HTTP requests are hanging then try removing this.
          .contentLength(Files.size(tar))
          .header("Content-Disposition",
              "attachment; filename=\"hardware.tar\"")
          .body(out -> {
            // This lambda should be executed on a separate thread and therefore
            // won't block this controller method.
            try {
              Files.copy(tar, out);
            } finally {
              // Important not to clean up before the response is written (e.g.
              // within the parent handler method).
              Files.delete(tar);
            }
          });
    } catch (ExecutionException | InterruptedException e) {
      // TODO: Should log instead of printing straight to stderr.
      e.printStackTrace();
      return ResponseEntity.internalServerError().build();
    }
  }

  public record ExecTarRequest(String message) {
  }
}
