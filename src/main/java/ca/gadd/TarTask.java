package ca.gadd;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import org.apache.tomcat.util.http.fileupload.FileUtils;

/**
 * Creates a tarball with two text files containing a caller-specified message.
 */
public class TarTask implements Callable<Path> {
  private final String message;

  /**
   * @param messsage This could be the {@link ExecTarRequest} directly depending
   *                 on how much separation of concern w.r.t. your controller.
   */
  public TarTask(String message) {
    this.message = message;
  }

  @Override
  public Path call() throws Exception {
    Path tempDir = null;
    try {
      tempDir = Files.createTempDirectory("tar");
      var file1 = Files.createFile(tempDir.resolve("file1.txt"));
      Files.writeString(file1, message, Charset.forName("UTF-8"));

      var file2 = Files.createFile(tempDir.resolve("file2.txt"));
      Files.writeString(file2, message, Charset.forName("UTF-8"));

      var tarFile = Files.createTempFile("tar", ".tar");

      // This obviously has environmental dependencies. You could replace this
      // with a pure Java implementation (take a look at commons-compress) if
      // you want. Though I'm not necessarily saying that's better.
      //
      // I'm using a files and disk because I don't know how much data you're
      // working with. If it's small, then doing everything in memory – and
      // maybe reaching for commons-compress in the process – would be speedy
      // and easier to work with, albeit less amenable to async requests.
      //
      // Also, this overall implementation is leaving the tar archive files with
      // a `tmp/<temp-dir-name>/` prefix. Addressing that is an exercise for the
      // reader. :P
      var proc = Runtime.getRuntime().exec(
          new String[] { "tar", "-czf", tarFile.toString(), tempDir.toString() });

      // TODO: Consider proc.waitFor(long, TimeUnit) to add a timeout.
      int exitCode = proc.waitFor();
      if (exitCode != 0) {
        // TODO: This implementation is bad. It's impossible to cleanup the tar
        // temporary file if something goes wrong here – though that file should
        // at least be empty.
        throw new IOException("Error writing tar archive");
      }

      return tarFile;
    } finally {
      if (tempDir != null) {
        FileUtils.deleteDirectory(tempDir.toFile());
      }
    }
  }
}
